'use strict';

describeComponent('component/data_saved_stops', function () {
  var addStopSpy;
  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
    // spy on local-storage methods
    spyOn(this.component, 'get');
    spyOn(this.component, 'set');
    addStopSpy = spyOnEvent(document, 'dataAddStop');
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on initialize', function() {
    it('triggers dataAddStop for each saved stop', function() {
      this.component.get.and.returnValue({
        'stop_id': {
          stop_id: 'stop_id'
        },
        'stop_id_2': {
          stop_id: 'stop_id_2'
        }
      });
      this.component.initialize(this.component.node);
      expect('dataAddStop').toHaveBeenTriggeredOn(
        document, {
          stop: {
            stop_id: 'stop_id'
          }
        }
      );
      expect('dataAddStop').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: {
            stop_id: 'stop_id_2'
          }
        });
    });
  });

  describe('on uiAddStop', function() {
    beforeEach(function() {
      this.component.trigger('uiAddStop', {
        stop: {
          stop_id: 'stop_id',
          route_id: 'route_id'
        }
      });
    });

    it('stores the stop locally', function() {
      expect(this.component.set).toHaveBeenCalledWith(
        'user-stops', {
          stop_id: {
            stop_id: 'stop_id',
            route_ids: ['route_id']
          }
        }
      );
    });

    it('triggers dataAddStop for the stop', function() {
      expect('dataAddStop').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: {
            stop_id: 'stop_id',
            route_ids: ['route_id']
          }
        });
    });

    it('stores multiple stops', function() {
      this.component.trigger('uiAddStop', {
        stop: {
          stop_id: 'stop_id_2',
          route_id: 'route_id_2'
        }
      });
      expect(this.component.set).toHaveBeenCalledWith(
        'user-stops', {
          'stop_id': {
            stop_id: 'stop_id',
            route_ids: ['route_id']
          },
          'stop_id_2': {
            stop_id: 'stop_id_2',
            route_ids: ['route_id_2']
          }
        });
    });

    describe('on an existing stop', function() {
      var updateStopSpy;
      beforeEach(function() {
        updateStopSpy = spyOnEvent(document, 'dataUpdateStop');

        this.component.trigger('uiAddStop', {
          stop: {
            stop_id: 'stop_id',
            route_id: 'route_id'
          }
        });
      });

      it('ignores it if the route is already present', function() {
        this.component.trigger('uiAddStop', {
          stop: {
            stop_id: 'stop_id',
            route_id: 'route_id'
          }
        });
        expect(addStopSpy.callCount).toEqual(1);
        expect(this.component.set.calls.count()).toEqual(1);
        expect(updateStopSpy).not.toHaveBeenTriggered();
      });

      describe('if present', function() {
        beforeEach(function() {
          this.component.trigger('uiAddStop', {
            stop: {
              stop_id: 'stop_id',
              route_id: 'route_id_2'
            }
          });
        });

        it('triggers dataUpdateStop with the new route', function() {
          expect(addStopSpy.callCount).toEqual(1);
          expect(updateStopSpy).toHaveBeenTriggeredOnAndWith(
            document,
            {
              stop: {
                stop_id: 'stop_id',
                route_ids: ['route_id', 'route_id_2']
              }
            });
        });

        it('saves the stop data', function() {
          expect(this.component.set).toHaveBeenCalledWith(
            'user-stops', {
              stop_id: {
                stop_id: 'stop_id',
                route_ids: ['route_id', 'route_id_2']
              }
            });
        });
      });
    });
  });

  describe('on uiUpdateStop', function() {
    beforeEach(function() {
      spyOnEvent(document, 'dataUpdateStop');
      this.component.trigger('uiUpdateStop', {
        stop: {
          stop_id: 'stop_id',
          route_ids: ['route a', 'route b']
        }
      });
    });

    it('saves the updated stop data', function() {
      expect(this.component.set).toHaveBeenCalledWith(
        'user-stops', {
          stop_id: {
            stop_id: 'stop_id',
            route_ids: ['route a', 'route b']
          }
        });
    });

    it('triggers dataUpdateStop', function() {
      expect('dataUpdateStop').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: {
            stop_id: 'stop_id',
            route_ids: ['route a', 'route b']
          }
        });
    });
  });

  describe('on uiRemoveStop', function() {
    beforeEach(function() {
      spyOnEvent(document, 'dataRemoveStop');
      this.component.trigger('uiAddStop', {
        stop: {
          stop_id: 'stop_id'
        }
      });
      this.component.trigger('uiAddStop', {
        stop: {
          stop_id: 'stop_id_2'
        }
      });
      this.component.trigger('uiRemoveStop', {
        stop: {
          stop_id: 'stop_id'
        }
      });

    });

    it('un-stores the stop locally', function() {
      expect(this.component.set).toHaveBeenCalledWith(
        'user-stops', {
          stop_id_2: {
            stop_id: 'stop_id_2',
            route_ids: [undefined]
          }
        });
    });

    it('triggers dataRemoveStop for the stop', function() {
      expect('dataRemoveStop').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: {
            stop_id: 'stop_id'
          }
        });
    });
  });
});
