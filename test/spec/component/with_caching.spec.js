'use strict';

describeMixin('component/with_caching', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
    window.localStorage.clear();
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('#getCache', function() {
    it('returns null if the value was not found', function() {
      expect(this.component.getCache('missing')).toBeNull();
    });

    it('returns the cached value if found', function() {
      this.component.setCache('found', {value: true});
      expect(this.component.getCache('found')).toEqual({value: true});
    });
  });
});
