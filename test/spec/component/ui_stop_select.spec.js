'use strict';

describeComponent('component/ui_stop_select', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent('<div><div class="stop-selector"></div><div class="route-selector"></div></div>', {
      routeSelector: '.route-selector',
      stopSelector: '.stop-selector'
    });
    spyOn($.fn, 'select2');
    jasmine.clock().install();
  });

  afterEach(jasmine.clock().uninstall);

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('initialize', function() {
    it('should request stops', function() {
      spyOnEvent(document, 'uiRequestStops');
      this.component.initialize(this.component.node);
      // trigger happens in the next interation of the event loop
      jasmine.clock().tick(1);
      expect('uiRequestStops').toHaveBeenTriggeredOn(document);
    });

    it('should request routes', function() {
      spyOnEvent(document, 'uiRequestRoutes');
      this.component.initialize(this.component.node);
      // trigger happens in the next interation of the event loop
      jasmine.clock().tick(1);
      expect('uiRequestRoutes').toHaveBeenTriggeredOn(document);
    });
  });

  describe('on dataRoutes', function() {
    it('gives routes to select2', function() {
      $(document).trigger('dataRoutes', {
        routes: [
          {route_id: 1,
           route_name: 'second',
           mode_name: 'Subway'
          },
          {route_id: 2,
           route_name: 'first',
           mode_name: 'Boat'
          }
        ]
      });
      expect($.fn.select2).toHaveBeenCalledWith({
        data: [
          {
            text: 'Subway',
            children: [
              {id: 1, text: 'second'}
            ]
          },
          {
            text: 'Boat',
            children: [
              {id: 2, text: 'first'}
            ]
          }
        ],
        placeholder: this.component.attr.routePlaceholder,
        searchInputPlaceholder: this.component.attr.searchInputPlaceholder,
        minimumInputLength: this.component.attr.minimumInputLength,
        width: this.component.attr.width
      });
    });
  });

  describe('on dataStopsForRoute', function() {
    it('gives sorted stops to select2', function() {
      $(document).trigger('dataStopsForRoute', {
        direction: [
          {
            direction_id: 5,
            direction_name: 'first direction',
            stop: [
              {stop_id: 1,
               stop_name: 'second',
               stop_order: 2
              },
              {stop_id: 2,
               stop_name: 'first',
               stop_order: 1
              }
            ]
          }
        ]
      });
      expect($.fn.select2).toHaveBeenCalledWith({
        data: [
          {
            text: 'first direction',
            children: [
              {id: 2, text: 'first', stop_order: 1},
              {id: 1, text: 'second', stop_order: 2}
            ]
          }
        ],
        placeholder: this.component.attr.placeholder,
        searchInputPlaceholder: this.component.attr.searchInputPlaceholder,
        minimumInputLength: this.component.attr.minimumInputLength,
        width: this.component.attr.width
      });
    });
  });

  describe('on route change', function() {
    it('triggers uiRequestStopsForRoute with the selected value', function() {
      spyOnEvent(this.component.node, 'uiRequestStopsForRoute');
      var ev = $.Event('change', {val: 2});
      this.component.select('routeSelector').trigger(ev);
      expect('uiRequestStopsForRoute').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {
          route: {
            route_id: 2
          }
        });
    });

    it('should write loading text to stop selector', function() {
      spyOnEvent(this.component.node, 'uiRequestStopsForRoute');
      var ev = $.Event('change', {val: 2});
      this.component.select('routeSelector').trigger(ev);
      expect(this.component.select('stopSelector').text()).toEqual(
        this.component.attr.loadingText);
    });

  });

  describe('on stop change', function() {
    it('resets the selected values', function() {
      this.component.select('stopSelector').change();
      expect($.fn.select2).toHaveBeenCalledWith('val', '');
    });

    it('triggers uiAddStop with the selected value', function() {
      spyOnEvent(this.component.node, 'uiAddStop');
      // route_id
      $.fn.select2.and.returnValue(5);
      var ev = $.Event('change', {val: 2});
      this.component.select('stopSelector').trigger(ev);
      expect($.fn.select2).toHaveBeenCalledWith('val');
      expect('uiAddStop').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {
          stop: {
            stop_id: 2,
            route_id: 5
          }
        });
    });
  });
});
