'use strict';

describeMixin('component/with_interval', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    jasmine.clock().install();
    this.setupComponent();
  });

  afterEach(jasmine.clock().uninstall);

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('#setInterval', function() {
    it('calls window.setInterval with the bound function', function() {
      var component = this.component;
      var done = false;

      function f() {
        expect(this).toBe(component);
        done = true;
      }

      this.component.setInterval(f, 1000);
      jasmine.clock().tick(1001);
      expect(done).toBeTruthy();
    });

  });

  describe('#clearInterval', function() {
    it('calls clearInterval to remove a set interval', function() {
      function f() { throw 'should not be called'; }

      var intervalID = this.component.setInterval(f, 1000);
      this.component.clearInterval(intervalID);
      jasmine.clock().tick(1001);
    });
  });

  describe('on teardown', function() {
    it('clears any active intervals', function() {
      function f() { throw 'should not be called'; }
      this.component.setInterval(f, 1000);
      this.component.teardown();
      jasmine.clock().tick(1001);
    });
  });
});
