'use strict';
define(['json!fixtures/alertsbystop.json'], function(AlertsFixture) {
  describeComponent('component/data_alerts', function () {
    var fixtureCopy;
    // Initialize the component and attach it to the DOM
    beforeEach(function () {
      fixtureCopy = $.extend(true, {}, AlertsFixture);
      fixtureCopy.alerts[0].effect_periods = [];
      fixtureCopy.alerts[1].effect_periods = [];
      this.setupComponent();
    });

    it('should be defined', function () {
      expect(this.component).toBeDefined();
    });

    describe('on serializeTiming', function() {
      it('does not return alerts for other routes', function() {
        expect(this.component.serializeTiming(fixtureCopy, {
          stop_id: 'stop_id',
          route_ids: ['non_existent']
        })).toEqual({
          stop_id: 'stop_id',
          alerts: []
        });
      });
      it('returns elevator alerts', function() {
        fixtureCopy.alerts[0].affected_services.elevators.push({
          elev_id: 'elevator_id'
        });
        expect(this.component.serializeTiming(fixtureCopy, {
          stop_id: 'stop_id',
          route_ids: []
        })).toEqual({
          stop_id: 'stop_id',
          alerts: [fixtureCopy.alerts[0]]
        });
      });
      it('returns alerts for tracked routes', function() {
        expect(this.component.serializeTiming(fixtureCopy, {
          stop_id: 'stop_id',
          route_ids: ['112']
        })).toEqual({
          stop_id: 'stop_id',
          alerts: [fixtureCopy.alerts[1]]
        });
      });

      it('ignores events that expired', function() {
        expect(this.component.serializeTiming(AlertsFixture, {
          stop_id: 'stop_id',
          route_ids: ['112']
        })).toEqual({
          stop_id: 'stop_id',
          alerts: []
        });
      });

      it('ignores events that start more than a day in the future', function() {
        spyOn(this.component, 'now').and.returnValue(
          1409603987 - (60 * 60 * 24) - 1);
        expect(this.component.serializeTiming(AlertsFixture, {
          stop_id: 'stop_id',
          route_ids: ['112']
        })).toEqual({
          stop_id: 'stop_id',
          alerts: []
        });
      });
    });
  });
});
