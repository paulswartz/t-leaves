'use strict';
define(
  ['json!fixtures/predictionsbystop.json'],
  function(PredictionFixture) {
    describeMixin('component/with_mbta_timing', function () {

      // Initialize the component and attach it to the DOM
      beforeEach(function () {
        var attributes = {
          listenEvent: 'listenEvent',
          triggerEvent: 'triggerEvent',
          apiEndpoint: 'apiEndpoint',
          msBetweenRequests: 10000
        };
        this.setupComponent(attributes);
        jasmine.clock().install();
        spyOn(this.component, 'getMbtaApi');

        // re-initialize to pick up the spied-upon getMbtaApi
        this.component.initialize(this.component.node, attributes);
      });

      afterEach(jasmine.clock().uninstall);

      it('should be defined', function () {
        expect(this.component).toBeDefined();
      });

      describe('on listenEvent', function() {
        it('asks the API for a timing', function() {
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 1
            }
          });
          expect(this.component.getMbtaApi).toHaveBeenCalledWith(
            'apiEndpoint',
            {
              stop: 1,
              success: jasmine.any(Function)
            });
        });

        it('does not make more than 1 request every 10 seconds', function() {
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 2
            }
          });
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 2
            }
          });
          expect(this.component.getMbtaApi.calls.count()).toEqual(1);
          jasmine.clock().tick(this.component.attr.msBetweenRequests + 1);
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 2
            }
          });
          expect(this.component.getMbtaApi.calls.count()).toEqual(2);
        });

        it('does make more than one request for different stops', function() {
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 3
            }
          });
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 4
            }
          });
          expect(this.component.getMbtaApi.calls.count()).toEqual(2);
        });

        it('immediately triggers a triggerEvent event if there is cached data', function() {
          spyOn(this.component, 'serializeTiming').and.returnValue('serialized');
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 5
            }
          });
          var success = this.component.getMbtaApi.calls.mostRecent().args[1].success;
          success.call(this.component, {stop_id: 5});

          // trigger again immediately, should receive the cached response
          spyOnEvent(document, 'triggerEvent');
          $(document).trigger('listenEvent', {
            stop: {
              stop_id: 5
            }
          });
          expect('triggerEvent').toHaveBeenTriggeredOnAndWith(
            document,
            {timing: 'serialized'});
        });
      });

      describe('on successful API response', function() {
        var stopData, success;
        beforeEach(function() {
          stopData = {
            stop_id: 6,
            route_ids: [5]
          };
          $(document).trigger('listenEvent', {
            stop: stopData
          });
          success = this.component.getMbtaApi.calls.mostRecent().args[1].success;
          spyOnEvent(document, 'triggerEvent');
        });

        it('triggers a triggerEvent event', function() {
          var timing = {stop_id: 6};
          spyOn(this.component, 'serializeTiming').and
            .returnValue('serialized');
          success.call(this.component, timing);
          expect('triggerEvent').toHaveBeenTriggeredOnAndWith(
            document, {timing: 'serialized'});
          expect(this.component.serializeTiming).toHaveBeenCalledWith(
            timing, stopData);
        });
      });

      describe('#serializeTiming', function() {
        var stop = {
          stop_id: '70069',
          route_ids: ['933_']
        };

        it('filters route_ids that are not in the stop data', function() {
          var timing = this.component.serializeTiming(
            PredictionFixture,
            stop);
          expect(timing.mode[0].route.length).toEqual(1);
          expect(timing.mode[0].route[0].route_id).toEqual('933_');
        });
      });
    });
  });
