'use strict';

describeComponent('component/ui_stop_container', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
    spyOnEvent(this.component.node, 'uiRequestPrediction');
    spyOnEvent(this.component.node, 'uiRequestSchedule');
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on dataAddStop', function() {
    var stop = {
      stop_id: 'North Station',
      route_ids: [3, 4]
    };
    beforeEach(function() {
      spyOn(this.component, 'attachChild');
      $(document).trigger('dataAddStop', {
        stop: stop
      });
    });
    it('adds a new element', function() {
      expect(this.component.$node.children().length).toEqual(1);
    });

    it('new element has appropriate classes', function() {
      expect(this.component.$node.children()[0]).toHaveClass(
        this.component.attr.stopClasses);
    });

    it('new component attached to that node', function() {
      expect(this.component.attachChild).toHaveBeenCalledWith(
        jasmine.any(Function), jasmine.any($), {
          stop: stop
        });
      // make sure we passed in the correct node
      expect(this.component.attachChild.calls.argsFor(0)[1][0]).toEqual(
        this.$node.children()[0]);
    });
  });
});
