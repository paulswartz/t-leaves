'use strict';

describeMixin('component/with_mbta_api', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('#getMbtaApi', function() {
    beforeEach(function() {
      spyOn(this.component, 'get');
    });

    it('makes a request to the MBTA API', function() {
      this.component.getMbtaApi('schedulebystop', {
        stop: 70069
      });
      expect(this.component.get).toHaveBeenCalledWith(
        {
          url: 'http://realtime.mbta.com/developer/api/v2/schedulebystop',
          data: {
            api_key: this.component.attr.mbtaApiKey,
            format: 'jsonp',
            stop: 70069
          },
          dataType: 'jsonp',
          jsonp: 'jsonpcallback',
          cache: true,
          success: undefined,
          error: undefined
        });
    });

    it('uses success and error callbacks if passed', function() {
      this.component.getMbtaApi('schedulebystop', {
        stop: 70069,
        success: function() {},
        error: function() {}
      });
      expect(this.component.get).toHaveBeenCalledWith(
        {
          url: 'http://realtime.mbta.com/developer/api/v2/schedulebystop',
          data: {
            api_key: this.component.attr.mbtaApiKey,
            format: 'jsonp',
            stop: 70069
          },
          dataType: 'jsonp',
          jsonp: 'jsonpcallback',
          cache: true,
          success: jasmine.any(Function),
          error: jasmine.any(Function)
        });
    });
  });

});
