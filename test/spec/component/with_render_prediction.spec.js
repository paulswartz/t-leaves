'use strict';

describeMixin('component/with_render_prediction', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('#renderTime', function() {
    it('returns the time rendered in minutes and seconds', function() {
      expect(this.component.renderTime(145)).toEqual('2:25');
    });
    it('returns seconds 0 padded', function() {
      expect(this.component.renderTime(61)).toEqual('1:01');
    });
    it('returns 0 for minutes if less than 60 seconds', function() {
      expect(this.component.renderTime(30)).toEqual('0:30');
    });

    it('correctly handles negative values', function() {
      expect(this.component.renderTime(-1)).toEqual('-0:01');
    });
  });

  describe('#renderDifference', function() {
    it('calls #renderTime with the difference in seconds between now and the given datetime', function() {
      var date = new Date();
      var dt = +date / 1000;
      var difference = 75;
      expect(this.component.renderDifference(dt + difference, date)).toEqual(
        this.component.renderTime(difference));
    });
  });

  describe('on click of toggleSelector', function() {
    beforeEach(function() {
      this.setupComponent(
        '<div><div class="stop" data-stop-id="7"><button class="edit"></button></div></div>',
        {
          toggleSelector: '.edit'
        });
    });

    it('triggers a uiToggleEditStop event', function() {
      spyOnEvent(this.component.node, 'uiToggleEditStop');
      this.$node.find('.stop').data('stop', 'stop data');
      this.component.select('toggleSelector').click();
      expect('uiToggleEditStop').toHaveBeenTriggeredOnAndWith(
        this.component.$node.find('.stop')[0],
        {
          stop: 'stop data'
        });
    });

    it('triggers a uiRequestRoutesForStop event', function() {
      spyOnEvent(this.component.node, 'uiRequestRoutesForStop');
      this.$node.find('.stop').data('stop', 'stop data');
      this.component.select('toggleSelector').click();
      expect('uiRequestRoutesForStop').toHaveBeenTriggeredOnAndWith(
        this.component.$node.find('.stop')[0],
        {
          stop: 'stop data'
        });
    });
  });

  describe('on click of deleteSelector', function() {
    it('triggers a uiRemoveStop event', function() {
      this.setupComponent(
        '<div><div class="stop"><button class="close"></button></div></div>',
        {
          deleteSelector: '.close'
        });
      this.$node.find('.stop').data('stop', 'stop data')
        .data('stop-id', 'stop id');
      spyOnEvent(this.component.node, 'uiRemoveStop');
      this.component.select('deleteSelector').click();
      expect('uiRemoveStop').toHaveBeenTriggeredOnAndWith(
        this.component.$node.find('.stop')[0],
        {
          stop: this.component.$node.find('.stop').data('stop')
        });
    });
  });

});
