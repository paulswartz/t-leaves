'use strict';

describeComponent('component/ui_help', function () {

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent('<div><div class="jumbotron"></div><div class="help-alert"><div class="help-text"></div></div></div>');
    spyOn(this.component, 'getCache');
    spyOn(this.component, 'setCache');
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on dataAddStop', function() {
    it('hides the jumbotron', function() {
      expect(this.component.select('jumbotronSelector')).toBeVisible();
      $(document).trigger('dataAddStop');
      expect(this.component.select('jumbotronSelector')).not.toBeVisible();
    });

    it('writes the edit text to the help alert', function() {
      $(document).trigger('dataAddStop');
      expect(this.component.select('helpTextSelector').html()).toEqual(
        this.component.attr.helpHTML.edit
      );
    });

    it('shows the edit alert the first time', function() {
      this.component.select('helpAlertSelector').hide();
      $(document).trigger('dataAddStop');
      expect(this.component.select('helpAlertSelector')).toBeVisible();
    });

    it('does not show the edit alert a second time', function() {
      this.component.getCache.and.returnValue(true);
      this.component.select('helpAlertSelector').hide();
      $(document).trigger('dataAddStop');
      expect(this.component.select('helpAlertSelector')).not.toBeVisible();
    });
  });

  describe('on uiToggleEdit', function() {
    it('shows the saving help', function() {
      this.component.select('helpAlertSelector').hide();
      this.component.$node.trigger('uiToggleEditStop');
      expect(this.component.select('helpTextSelector').html()).toEqual(
        this.component.attr.helpHTML.saving);
      expect(this.component.select('helpAlertSelector')).toBeVisible();
    });
    it('shows the questions help the second time', function() {
      this.component.getCache.and.callFake(function(type) {
        return type === 'saving';
      });
      this.component.$node.trigger('uiToggleEditStop');
      expect(this.component.select('helpAlertSelector')).toBeVisible();
      expect(this.component.select('helpTextSelector').html()).toEqual(
        this.component.attr.helpHTML.questions);
    });
  });
});
