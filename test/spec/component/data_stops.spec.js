'use strict';

describeComponent('component/data_stops', function () {
  var stops = {
    'direction': [
      {
        'stop': [
          {
            'stop_lon': '-70.991648',
            'stop_lat': '42.41342',
            'parent_station_name': 'Wonderland',
            'parent_station': 'place-wondl',
            'stop_name': 'Wonderland',
            'stop_id': '70060',
            'stop_order': '1'
          },
          {
            'stop_lon': '-70.99253321',
            'stop_lat': '42.40784254',
            'parent_station_name': 'Revere Beach',
            'parent_station': 'place-rbmnl',
            'stop_name': 'Revere Beach - Inbound',
            'stop_id': '70057',
            'stop_order': '2'
          },
          {
            'stop_lon': '-71.062037',
            'stop_lat': '42.361365',
            'parent_station_name': 'Bowdoin',
            'parent_station': 'place-bomnl',
            'stop_name': 'Bowdoin',
            'stop_id': '70038',
            'stop_order': '12'
          }
        ],
        'direction_name': 'Outbound',
        'direction_id': '0'
      },
      {
        'stop': [
          {
            'stop_lon': '-71.062037',
            'stop_lat': '42.361365',
            'parent_station_name': 'Bowdoin',
            'parent_station': 'place-bomnl',
            'stop_name': 'Bowdoin',
            'stop_id': '70038',
            'stop_order': '1'
          }
        ],
        'direction_name': 'Inbound',
        'direction_id': '1'
      }
    ]
  };

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
    spyOn(this.component, 'getCache');
    spyOn(this.component, 'setCache');
    spyOn(this.component, 'getMbtaApi');
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on uiRequestStopsForRoute', function() {
    it('requests the stops from the server if not cached', function() {
      $(document).trigger('uiRequestStopsForRoute', {
        route: {
          route_id: 'abc'
        }
      });
      expect(this.component.getMbtaApi).toHaveBeenCalledWith(
        'stopsbyroute', {
          route: 'abc',
          success: jasmine.any(Function)
        });
    });

    it('calls onStopsForRoute with the cached data if present', function() {
      spyOn(this.component, 'onStopsForRoute');
      var route = {
        route_id: 'abc'
      };
      var data = {here: 'is some data'};
      this.component.getCache.and.returnValue(data);
      $(document).trigger('uiRequestStopsForRoute', {route: route});
      expect(this.component.getCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey + 'abc');
      expect(this.component.onStopsForRoute).toHaveBeenCalledWith(data, route);
    });
  });

  describe('#onStopsForRoute', function() {
    it('caches the response', function() {
      var data = {direction: []};
      this.component.onStopsForRoute(data, {route_id: 'bcd'});
      expect(this.component.setCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey + 'bcd', data, 86400);
    });

    it('includes other data present in the response', function() {
      var data = {direction: [],
                  other: 'data'};
      spyOnEvent(this.component.node, 'dataStopsForRoute');
      this.component.onStopsForRoute(data, {route_id: 'abc'});
      expect('dataStopsForRoute').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {other: 'data',
         direction: []});
    });

    it('triggers a dataStopsForRoute event with the serialized routes', function() {
      spyOnEvent(this.component.node, 'dataStopsForRoute');
      this.component.onStopsForRoute(stops, {route_id: 'efg'});
      var expected = {
        direction: [
          {
            direction_id: '0',
            direction_name: 'Outbound',
            stop: [
              {
                stop_name: 'Wonderland',
                stop_id: '70060',
                stop_order: 1,
                stop_lon: '-70.991648',
                stop_lat: '42.41342',
                parent_station_name: 'Wonderland',
                parent_station: 'place-wondl'

              },
              {
                stop_name: 'Revere Beach - Inbound',
                stop_id: '70057',
                stop_order: 2,
                stop_lon: '-70.99253321',
                stop_lat: '42.40784254',
                parent_station_name: 'Revere Beach',
                parent_station: 'place-rbmnl'
              },
              {
                stop_name: 'Bowdoin',
                stop_id: '70038',
                stop_order: 12,
                stop_lon: '-71.062037',
                stop_lat: '42.361365',
                parent_station_name: 'Bowdoin',
                parent_station: 'place-bomnl'

              }
            ]
          },
          {
            direction_id: '1',
            direction_name: 'Inbound',
            stop: [
              {
                stop_name: 'Bowdoin',
                stop_id: '70038',
                stop_order: 1,
                stop_lon: '-71.062037',
                stop_lat: '42.361365',
                parent_station_name: 'Bowdoin',
                parent_station: 'place-bomnl'
              }
            ]
          }
        ]
      };
      expect('dataStopsForRoute').toHaveBeenTriggeredOnAndWith(
        this.component.node, expected);
    });
  });
});
