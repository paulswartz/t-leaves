'use strict';

describeComponent('component/data_routes', function () {
  var routes = {
    'mode': [
      {
        'route': [
          {
            'route_name': 'Blue Line',
            'route_id': '946_'
          },
          {
            'route_name': 'Blue Line',
            'route_id': '948_'
          },
          {
            'route_name': 'Orange Line',
            'route_id': '903_'
          },
          {
            'route_name': 'Orange Line',
            'route_id': '913_'
          },
          {
            'route_name': 'Red Line',
            'route_id': '931_'
          },
          {
            'route_name': 'Red Line',
            'route_id': '933_'
          }
        ],
        'mode_name': 'Subway',
        'route_type': '1'
      }
    ]
  };

  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    this.setupComponent();
    spyOn(this.component, 'getCache');
    spyOn(this.component, 'setCache');
    spyOn(this.component, 'getMbtaApi');
  });

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on uiRequestRoutes', function() {
    it('requests the routes from the server if not cached', function() {
      $(document).trigger('uiRequestRoutes');
      expect(this.component.getMbtaApi).toHaveBeenCalledWith(
        'routes', {
          success: this.component.onRoutes
        });
    });

    it('calls onRoutes with the cached data if present', function() {
      spyOn(this.component, 'onRoutes');
      var data = {here: 'is some data'};
      this.component.getCache.and.returnValue(data);
      $(document).trigger('uiRequestRoutes');
      expect(this.component.getCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey);
      expect(this.component.onRoutes).toHaveBeenCalledWith(data);
    });
  });

  describe('on uiRequestRoutesForStop', function() {
    var request = {
      stop: {
        stop_id: 8
      }
    };
    it('requests the routes from the server if not cached', function() {
      $(document).trigger('uiRequestRoutesForStop', request);
      expect(this.component.getMbtaApi).toHaveBeenCalledWith(
        'routesbystop', {
          stop: 8,
          success: this.component.onRoutesForStop
        });
    });

    it('calls onRoutesForStop with the cached data if present', function() {
      var response = {stop_id: 8, more: 'data'};
      spyOn(this.component, 'onRoutesForStop');
      this.component.getCache.and.returnValue(response);
      $(document).trigger('uiRequestRoutesForStop', request);
      expect(this.component.getCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey + '-8');
      expect(this.component.onRoutesForStop).toHaveBeenCalledWith(response);
    });
  });

  describe('#onRoutes', function() {
    it('caches the response', function() {
      var data = {mode: []};
      this.component.onRoutes(data);
      expect(this.component.setCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey, data, 86400);
    });

    it('includes other data present in the response', function() {
      var data = {mode: [],
                  other: 'data'};
      spyOnEvent(this.component.node, 'dataRoutes');
      this.component.onRoutes(data);
      expect('dataRoutes').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {other: 'data',
         routes: []});
    });

    it('triggers a dataRoutes event with the serialized routes', function() {
      spyOnEvent(this.component.node, 'dataRoutes');
      this.component.onRoutes(routes);
      expect('dataRoutes').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {
          routes: [
            {route_id: '946_',
             route_name: 'Blue Line (Orient Heights)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '948_',
             route_name: 'Blue Line (Wonderland)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '903_',
             route_name: 'Orange Line (Oak Grove)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '913_',
             route_name: 'Orange Line (Wellington)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '931_',
             route_name: 'Red Line (Ashmont)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '933_',
             route_name: 'Red Line (Braintree)',
             route_type: '1',
             mode_name: 'Subway'},
          ]
        });
    });
  });

  describe('#onRoutesForStop', function() {
    it('caches the response', function() {
      var data = {stop_id: 9, mode: []};
      this.component.onRoutesForStop(data);
      expect(this.component.setCache).toHaveBeenCalledWith(
        this.component.attr.cacheKey + '-9', data, 86400);
    });

    it('includes stop in the response', function() {
      var data = {mode: [],
                  stop_id: 9,
                  stop_name: 'stop_name'};
      spyOnEvent(this.component.node, 'dataRoutesForStop');
      this.component.onRoutesForStop(data);
      expect('dataRoutesForStop').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {stop: {
          stop_id: 9,
          stop_name: 'stop_name'
        },
         routes: []});
    });

    it('triggers a dataRoutesForStop event with the serialized routes', function() {
      spyOnEvent(this.component.node, 'dataRoutesForStop');
      this.component.onRoutesForStop(
        $.extend(
          {stop_id: 9, stop_name: 'stop_name'},
          routes
        ));
      expect('dataRoutesForStop').toHaveBeenTriggeredOnAndWith(
        this.component.node,
        {
          stop: {
            stop_id: 9,
            stop_name: 'stop_name'
          },
          routes: [
            {route_id: '946_',
             route_name: 'Blue Line (Orient Heights)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '948_',
             route_name: 'Blue Line (Wonderland)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '903_',
             route_name: 'Orange Line (Oak Grove)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '913_',
             route_name: 'Orange Line (Wellington)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '931_',
             route_name: 'Red Line (Ashmont)',
             route_type: '1',
             mode_name: 'Subway'},
            {route_id: '933_',
             route_name: 'Red Line (Braintree)',
             route_type: '1',
             mode_name: 'Subway'},
          ]
        });
    });
  });
});
