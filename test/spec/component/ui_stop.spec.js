'use strict';

describeComponent('component/ui_stop', function () {
  // Initialize the component and attach it to the DOM
  beforeEach(function () {
    spyOnEvent(document, 'uiRequestPrediction');
    spyOnEvent(document, 'uiRequestSchedule');
    spyOnEvent(document, 'uiRequestAlerts');

    this.setupComponent({
      stop: {
        stop_id: 'North Station',
        route_ids: [1, 2]
      }
    });
    spyOn(this.component, 'renderRouteInto');
    jasmine.clock().install();
  });

  afterEach(jasmine.clock().uninstall);

  it('should be defined', function () {
    expect(this.component).toBeDefined();
  });

  describe('on initialize', function() {
    it('triggers a request for predictions', function() {
      expect('uiRequestPrediction').toHaveBeenTriggeredOnAndWith(
        document,
        {
          stop: this.component.attr.stop
        });
    });
    it('triggers a request for schedules', function() {
      expect('uiRequestSchedule').toHaveBeenTriggeredOnAndWith(
        document,
        {
          stop: this.component.attr.stop
        });
    });
    it('triggers a request for alerts', function() {
      expect('uiRequestAlerts').toHaveBeenTriggeredOnAndWith(
        document,
        {
          stop: this.component.attr.stop
        });
    });
  });

  describe('on dataUpdateStop', function() {
    var data = {
      stop_id: 'North Station',
      other: 'data'
    };
    beforeEach(function() {
      $(document).trigger('dataUpdateStop', {stop: data});
    });

    it('updates the state', function() {
      expect(this.component.state.stop).toEqual(data);
    });

    it('requests predictions', function() {
      expect('uiRequestPrediction').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: this.component.state.stop
        });
    });

    it('requests schedules', function() {
      expect('uiRequestSchedule').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: this.component.state.stop
        });
    });

    it('re-renders the route in the next animation frame', function(done) {
      window.requestAnimationFrame(function() {
        expect(this.component.renderRouteInto).toHaveBeenCalledWith(
          this.$node);
        done();
      }.bind(this));
    });

    it('ignores update for a different stop', function() {
      var previousState = this.component.state.stop;
      $(document).trigger('dataUpdateStop', {stop: {
        stop_id: 'other stop'
      }});
      expect(this.component.state.stop).toEqual(previousState);
    });
  });

  describe('on dataPrediction', function() {
    var data = {
      timing: {
        stop_id: 'North Station'
      }
    };
    it('updates the state', function() {
      $(document).trigger('dataPrediction', data);
      expect(this.component.state.prediction).toEqual(data.timing);
    });
    it('ignores update for a different stop', function() {
      var previousState = this.component.state.prediction;
      $(document).trigger('dataPrediction', {timing: {
        stop_id: 'other stop'
      }});
      expect(this.component.state.prediction).toEqual(previousState);
    });
  });

  describe('on dataSchedule', function() {
    var data = {
      timing: {
        stop_id: 'North Station'
      }
    };
    it('updates the state', function() {
      $(document).trigger('dataSchedule', data);
      expect(this.component.state.schedule).toEqual(data.timing);
    });
    it('ignores update for a different stop', function() {
      var previousState = this.component.state.schedule;
      $(document).trigger('dataSchedule', {timing: {
        stop_id: 'other stop'
      }});
      expect(this.component.state.schedule).toEqual(previousState);
    });
  });

  describe('on uiToggleEditStop', function() {
    it('toggles the edit flag on the state', function() {
      this.component.trigger('uiToggleEditStop');
      expect(this.component.state.editing).toBeTruthy();
      this.component.trigger('uiToggleEditStop');
      expect(this.component.state.editing).toBeFalsy();
    });
  });

  describe('on dataRemoveStop', function() {
    it('tears down the component', function() {
      spyOn(this.component, 'teardown').and.callThrough();
      $(document).trigger('dataRemoveStop', {
        stop: this.component.state.stop
      });
      expect(this.component.teardown).toHaveBeenCalledWith();
    });
    it('removes the associated element', function() {
      spyOn(this.component.$node, 'remove').and.callThrough();
      $(document).trigger('dataRemoveStop', {
        stop: this.component.state.stop
      });
      expect(this.component.$node.remove).toHaveBeenCalledWith();
    });
  });

  describe('#everyThirtySeconds', function() {
    it('schedules a trigger of uiRequestPrediction in the future', function() {
      spyOnEvent(document, 'uiRequestPrediction');
      this.component.everyThirtySeconds();
      expect('uiRequestPrediction').not.toHaveBeenTriggered();
      jasmine.clock().tick(1001);
      expect('uiRequestPrediction').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: this.component.state.stop
        });
    });

    it('schedules a trigger of uiRequestSchedule in the future', function() {
      spyOnEvent(document, 'uiRequestSchedule');
      this.component.everyThirtySeconds();
      expect('uiRequestSchedule').not.toHaveBeenTriggered();
      jasmine.clock().tick(1001);
      expect('uiRequestSchedule').toHaveBeenTriggeredOnAndWith(
        document, {
          stop: this.component.state.stop
        });
    });

    it('does not trigger requests if the page is not visible', function() {
      spyOnEvent(document, 'uiRequestSchedule');
      spyOnEvent(document, 'uiRequestPrediction');
      $(document).trigger('visibility-change', {state: 'hidden'});
      this.component.everyThirtySeconds();
      expect('uiRequestSchedule').not.toHaveBeenTriggered();
      expect('uiRequestPrediction').not.toHaveBeenTriggered();
    });
  });
});
