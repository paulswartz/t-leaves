define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var precompiledTemplates = require('templates/compiled');
  var hoganPrecompiled = require('flight-hogan/lib/hogan_precompiled');

  var StopSelect = require('component/ui_stop_select');
  var StopContainer = require('component/ui_stop_container');
  var Help = require('component/ui_help');
  var Select2Scroll = require('component/ui_select2_scroll');
  var Routes = require('component/data_routes');
  var Stops = require('component/data_stops');
  var SavedStops = require('component/data_saved_stops');
  var Predictions = require('component/data_predictions');
  var Schedule = require('component/data_schedule');
  var Alerts = require('component/data_alerts');
  var Visibility = require('flight-visibility/lib/visibility');

  require('components-bootstrap/js/bootstrap');

  /**
   * Module exports
   */

  return initialize;

  /**
   * Module function
   */

  function initialize() {
    hoganPrecompiled.attachTo(document, {
      precompiledTemplates: precompiledTemplates
    });

    Help.attachTo(document.body);
    Select2Scroll.attachTo(window);
    StopSelect.attachTo('.search-group');
    StopContainer.attachTo('.stop-group');
    Routes.attachTo(document);
    Stops.attachTo(document);
    Predictions.attachTo(document);
    Schedule.attachTo(document);
    Alerts.attachTo(document);
    Visibility.attachTo(document);
    SavedStops.attachTo(document);

    if (window.location.hash === '#reset') {
      if (window.confirm('Are you sure you want to reset all T-Leaves data?')) {
        window.localStorage.clear();
        window.location.hash = '';
      }
    }
  }

});
