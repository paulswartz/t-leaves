'use strict';

requirejs.config({
  baseUrl: 'bower_components',
  paths: {
    'component': '../js/component',
    'page': '../js/page',
    'templates': '../templates',
    'hoganjs': 'hogan/web/builds/3.0.2/hogan-3.0.2.amd'
  },
  map: {
    'templates/compiled': { 'hogan': 'hoganjs'}
  }
});

require(
  [
    'flight/lib/compose',
    'flight/lib/registry',
    'flight/lib/advice',
    'flight/lib/logger',
    'flight/lib/debug'
  ],

  function(compose, registry, advice, withLogging, debug) {
    var hasDataMain = $('script[data-main]').length;
    debug.enable(hasDataMain);
    compose.mixin(registry, [advice.withAdvice]);

    require(['page/default'], function(initializeDefault) {
      initializeDefault();
    });
  }
);
