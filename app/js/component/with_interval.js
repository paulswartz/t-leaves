define(function () {

  'use strict';

  /**
   * Module exports
   */

  return withInterval;

  /**
   * Module function
   */

  function withInterval() {
    this.attributes({

    });

    var intervals = [];

    this.setInterval = function(func, timeout) {
      var intervalID = setInterval(func.bind(this), timeout);
      intervals.push(intervalID);
      return intervalID;
    };

    this.clearInterval = function(intervalID) {
      clearInterval(intervalID);
      var index = intervals.indexOf(intervalID);
      if (index > -1) {
        intervals.splice(index, 1);
      }
    };

    this.after('teardown', function () {
      intervals.forEach(clearInterval);
      intervals = [];
    });
  }

});
