define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withMbtaTiming = require('component/with_mbta_timing');

  /**
   * Module exports
   */

  // MbtaTiming goes first so that we override the attributes
  return defineComponent(withMbtaTiming, dataPredictions);

  /**
   * Module function
   */

  function dataPredictions() {
    this.attributes({
      listenEvent: 'uiRequestPrediction',
      triggerEvent: 'dataPrediction',
      apiEndpoint: 'predictionsbystop',
      apiData: {
        include_service_alerts: false
      },
      msBetweenRequests: 10000
    });
  }

});
