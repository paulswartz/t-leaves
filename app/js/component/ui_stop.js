define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component'),
      withState = require('flight-with-state/lib/with-state'),
      withInterval = require('component/with_interval'),
      withBatch = require('flight-with-batch/lib/with-batch'),
      withRenderPrediction = require('component/with_render_prediction');

  /**
   * Module exports
   */
  return defineComponent(withState, withBatch, withInterval,
                         withRenderPrediction, uiStop);

  /**
   * Module function
   */

  function uiStop() {
    this.attributes({
      stop: null,
      routeSelector: '.route-control',
      tripsSelector: '.trips',
      toggleSelector: '.close',
      deleteSelector: '.btn-delete',
      maximumNumberOfTrips: 5
    });

    this.initialState({
      editing: false,
      stop: this.fromAttr('stop'),
      allRoutes: null,
      predictions: null,
      schedules: null,
      alerts: null,
      visible: true
    });

    this.around('stateFor', function stateFor() {
      return this.state;
    });

    this.onStateChanged = function() {
      this.renderRouteInto(this.$node);
    };

    this.everySecond = function() {
      if (!this.state.visible) {
        return;
      }
      this.batch(function() {
        this.renderTripsInto(this.$node);
      });
    };

    this.everyThirtySeconds = function() {
      if (!this.state.visible) {
        return;
      }
      var data = {
        stop: this.state.stop
      };
      $(document).trigger('uiRequestPrediction', data);
      $(document).trigger('uiRequestSchedule', data);
    };

    this.everyFiveMinutes = function() {
      var data = {
        stop: this.state.stop
      };
      $(document).trigger('uiRequestAlerts', data);
      if (!this.state.visible) {
        $(document).trigger('uiRequestPrediction', data);
        $(document).trigger('uiRequestSchedule', data);
      }
    };

    this.onUpdateStop = function(ev, data) {
      if (data.stop.stop_id !== this.state.stop.stop_id) {
        return;
      }
      this.mergeState({
        stop: data.stop
      });
      data = {
        stop: this.state.stop
      };
      $(document).trigger('uiRequestPrediction', data);
      $(document).trigger('uiRequestSchedule', data);
    };

    this.onRemoveStop = function(ev, data) {
      if (data.stop.stop_id !== this.state.stop.stop_id) {
        return;
      }
      this.teardown();
      this.$node.remove();
    };

    function createStateListener(dataKey) {
      return function(ev, data) {
        if (data.timing.stop_id !== this.state.stop.stop_id) {
          return;
        }
        var state = {};
        state[dataKey] = data.timing;
        this.mergeState(state);
      };
    }

    this.onPrediction = createStateListener('prediction');
    this.onSchedule = createStateListener('schedule');

    this.onAlerts = function(ev, data) {
      if (data.alerts.stop_id !== this.state.stop.stop_id) {
        return;
      }
      this.mergeState({
        alerts: data.alerts
      });
    };

    this.onToggleEditStop = function() {
      this.mergeState({
        editing: !this.state.editing
      });
    };

    this.onRoutesForStop = function(ev, data) {
      if (data.stop.stop_id !== this.state.stop.stop_id) {
        return;
      }
      this.mergeState({
        allRoutes: data
      });
    };

    this.onVisibilityChange = function(ev, data) {
      this.mergeState({
        visible: data.state === 'visible'
      });

      if (this.state.visible) {
        this.everySecond();
        this.everyThirtySeconds();
        this.everyFiveMinutes();
      }
    };

    this.after('initialize', function () {
      this.after('stateChanged', this.onStateChanged);

      // data events
      this.on(document, 'dataUpdateStop', this.onUpdateStop);
      this.on(document, 'dataRemoveStop', this.onRemoveStop);
      this.on(document, 'dataPrediction', this.onPrediction);
      this.on(document, 'dataSchedule', this.onSchedule);
      this.on(document, 'dataAlerts', this.onAlerts);
      this.on(document, 'dataRoutesForStop', this.onRoutesForStop);
      this.on(document, 'visibility-change', this.onVisibilityChange);
      this.on('uiToggleEditStop', this.onToggleEditStop);

      // intervals
      this.setInterval(this.everySecond,
                       1000);
      this.setInterval(this.everyThirtySeconds,
                       1000 * 30);
      this.setInterval(this.everyFiveMinutes,
                       1000 * 60 * 5);

      // get data
      var data = {
        stop: this.attr.stop
      };
      this.trigger('uiRequestPrediction', data);
      this.trigger('uiRequestSchedule', data);
      this.trigger('uiRequestAlerts', data);

    });
  }

});
