define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withMbtaTiming = require('component/with_mbta_timing');

  /**
   * Module exports
   */

  return defineComponent(withMbtaTiming, dataAlerts);

  /**
   * Module function
   */

  function dataAlerts() {
    this.attributes({
      listenEvent: 'uiRequestAlerts',
      triggerEvent: 'dataAlerts',
      apiEndpoint: 'alertsbystop',
      apiData: {
        include_access_alerts: true
      },
      msBetweenRequests: 1000 * 60 * 5,
      dataKey: 'alerts'
    });

    // hook for testing
    this.now = function() {
      return +new Date() / 1000;
    };

    // it's not quite timing, but it's close enough that we're using the
    // mixin
    this.around('serializeTiming',  function(ignored, data, stop) {
      var alerts = {
        stop_id: stop.stop_id,
        alerts: []
      },
          now = this.now();

      data.alerts.forEach(function(alert) {
        var affected = false, ignored = false, i;
        alert.effect_periods.forEach(function(period) {
          // ignore events in the past
          if (period.effect_end && parseInt(period.effect_end, 10) < now) {
            ignored = true;
          }
          // ignore events from more than a day in the future
          if (period.effect_start &&
              parseInt(period.effect_start) - now > 60 * 60 * 24) {
            ignored = true;
          }
        });
        if (ignored) {
          return;
        }
        if (alert.affected_services.elevators.length) {
          // elevator alerts always affect the stop
          affected = true;
        } else {
          for (i=0; i < alert.affected_services.services.length; i++) {
            if (stop.route_ids.indexOf(alert.affected_services.services[i].route_id) > -1) {
              affected = true;
              break;
            }
          }
        }
        if (affected) {
          alerts.alerts.push(alert);
        }
      });
      return alerts;
    });
  }

});
