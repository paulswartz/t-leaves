define(function (require) {

  'use strict';

  var compose = require('flight/lib/compose');
  var withRequest = require('flight-request/lib/with_request');

  /**
   * Module exports
   */

  return withMbtaApi;

  /**
   * Module function
   */

  function withMbtaApi() {
    compose.mixin(this, [withRequest]);

    this.attributes({
      mbtaApiKey: 'l6rv7DcXcUOBgf6lnm3_qg',
      mbtaRootUrl: 'http://realtime.mbta.com/developer/api/v2/'
    });

    this.getMbtaApi = function(name, params) {
      params = params || {};
      var success, error;
      if (params.success) {
        success = params.success.bind(this);
        delete params.success;
      }
      if (params.error) {
        error = params.error.bind(this);
        delete params.error;
      }
      return this.get(
        {
          url: this.attr.mbtaRootUrl + name,
          data: $.extend({
            api_key: this.attr.mbtaApiKey,
            format: 'jsonp'
          }, params),
          dataType: 'jsonp',
          jsonp: 'jsonpcallback',
          cache: true,
          success: success,
          error: error
        });
    };
  }

});
