define(function (require) {

  'use strict';
  var LocalStorage = require('flight-storage/lib/adapters/local-storage');

  /**
   * Module exports
   */

  return withCaching;

  /**
   * Module function
   */

  function withCaching() {
    var storage = new LocalStorage();
    // increment this in order to get the cache refreshed
    var version = 1;

    this.attributes({
      cachingPrefix: 'cache-'
    });

    this.getCache = function(key) {
      var value = storage.get(this.attr.cachingPrefix + key) || null;
      if (!value) {
        return value;
      }
      if (typeof value !== 'object') {
        return null;
      }
      if (value._version !== version) {
        return null;
      }
      if (value.expirationTime &&
          value.expirationTime > +new Date()) {
        return null;
      }
      return value.value;
    };

    this.setCache = function(key, value, expirationTime) {
      expirationTime = expirationTime && +new Date() + expirationTime * 1000;
      value = {
        _version: version,
        expirationTime: expirationTime,
        value: value
      };
      storage.set(this.attr.cachingPrefix + key, value);
    };
  }

});
