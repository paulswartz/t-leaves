define(function (require) {

  'use strict';

  var compose = require('flight/lib/compose');
  var withHogan = require('flight-hogan/lib/with_hogan');
  /**
   * Module exports
   */

  return withRenderPrediction;

  /**
   * Module function
   */

  function withRenderPrediction() {
    compose.mixin(this, [withHogan]);

    this.attributes({
      stopSelector: '.stop',
      routeSelector: '.route-control',
      tripsSelector: '.trips',
      toggleSelector: '.close',
      deleteSelector: '.btn-delete',
      maximumNumberOfTrips: 5
    });

    this.renderTime = function(total) {
      var prefix = '';
      if (total < 0) {
        prefix = '-';
        total = -total;
      }
      var minutes = Math.floor(total / 60);
      var seconds = Math.floor(total - (minutes * 60));
      if (seconds < 10) {
        seconds = '0' + seconds;
      }
      return prefix + minutes + ':' + seconds;
    };

    this.renderDifference = function(dt, now) {
      now = now || new Date();
      dt = parseInt(dt, 10);
      if (isNaN(dt)) {
        return null;
      }
      now = Math.floor(+now / 1000);
      return this.renderTime(dt - now);
    };

    this.panelClass = function(timing) {
      var className = 'panel-default';
      var map = {
        'Green Line': 'panel-success',
        'Red Line': 'panel-danger',
        'Blue Line': 'panel-info',
        'Orange Line': 'panel-warning'
      };
      timing.mode.forEach(function(mode) {
        mode.route.forEach(function(route) {
          if (map[route.route_name]) {
            className = map[route.route_name];
          }
        });
      });
      return className;
    };

    this.alertClass = function(alert) {
      if (alert.severity === 'Severe') {
        return 'text-danger';
      } else if (alert.severity === 'Moderate') {
        return '';
      } else {
        return 'text-muted';
      }
    };

    this.tripHeadsign = function(trip) {
      if (trip.trip_headsign) {
        return trip.trip_headsign;
      }
      var nameRegex = /(\d+:\d+ .{2}) from (.*) to (.*)/;
      var match = nameRegex.exec(trip.trip_name);
      if (match) {
        return match[3];
      }
      return null;
    };

    this.stateFor = function stateFor($el) {
      return {
        stop: $el.data('stop'),
        stopId: $el.data('stop-id').toString().replace(/_/g, ' '),
        prediction: $el.data('prediction'),
        schedule: $el.data('schedule'),
        editing: $el.data('editing'),
        alerts: $el.data('alerts'),
        allRoutes: $el.data('all-routes')
      };
    };

    this.serializeTrips = function(state) {
      var prediction = state.prediction;
      var schedule = state.schedule;
      var trips = {}, tripsArray = [];
      var timings = [];

      // prefer the prediction to the schedule
      if (schedule) {
        timings.push(schedule);
      }
      if (prediction) {
        timings.push(prediction);
      }

      timings.forEach(function(timing) {
        timing.mode.forEach(function(mode) {
          mode.route.forEach(function(route) {
            route.direction.forEach(function(direction) {
              direction.trip.forEach(function(trip) {
                trips[trip.trip_id] = {
                  mode_name: mode.mode_name,
                  route_id: route.route_id,
                  route_name: route.route_name,
                  direction_name: direction.direction_name,
                  trip_id: trip.trip_id,
                  trip_name: trip.trip_name,
                  trip_headsign: this.tripHeadsign(trip),
                  scheduled_arrival: this.renderDifference(
                    trip.sch_arr_dt),
                  predicted_arrival: this.renderDifference(
                    trip.pre_dt),
                  best_arrival: parseInt(trip.pre_dt || trip.sch_arr_dt, 10)
                };
              }, this);
            }, this);
          }, this);
        }, this);
      }, this);

      for (var trip_id in trips) {
        if (trips.hasOwnProperty(trip_id)) {
          // don't show expired schedules
          if (trips[trip_id].best_arrival >= +new Date() / 1000) {
            tripsArray.push(trips[trip_id]);
          }
        }
      }

      tripsArray.sort(function(a, b) {
        return a.best_arrival - b.best_arrival;
      });

      if (this.attr.maximumNumberOfTrips) {
        tripsArray = tripsArray.slice(0, this.attr.maximumNumberOfTrips);
      }

      return tripsArray;
    };

    this.serializeAlerts = function(state) {
      var alerts = [];
      if (!state.alerts) {
        return false;
      }
      state.alerts.alerts.forEach(function(alert) {
        alerts.push({
          short_header_text: alert.short_header_text,
          severity: alert.severity,
          cssClass: this.alertClass(alert)
        });
      }, this);

      if (!alerts.length) {
        return false;
      }

      alerts.sort(function(a, b) {
        var levels = ['Severe', 'Moderate', 'Minor'];
        a = levels.indexOf(a.severity);
        b = levels.indexOf(b.severity);
        return a - b;
      });

      return {alerts: alerts};
    };

    this.serializePrediction = function(state) {
      var timing = state.prediction || state.schedule;
      if (!timing) {
        return {};
      }
      return {
        stop_id: timing.stop_id,
        stop_name: timing.stop_name,
        panel_class: this.panelClass(timing),
        editing: state.editing,
        trips: this.serializeTrips(state),
        alerts: this.serializeAlerts(state)
      };
    };

    this.renderTrips = function(state) {
      return this.renderTemplate('trips', {
        trips: this.serializeTrips(state)
      });
    };

    this.renderRouteInto = function($el) {
      var state = this.stateFor($el);
      $el.html(this.renderTemplate(
        'prediction',
        this.serializePrediction(state)));
      this.renderRouteSelectorInto($el);
    };

    this.renderTripsInto = function($el) {
      // if we've already rendered the prediction, just update the trips
      if ($el.children().length) {
        var state = this.stateFor($el);
        $el.find(this.attr.tripsSelector).html(
          this.renderTrips(state));
      } else {
        this.renderRouteInto($el);
      }
    };

    this.renderRouteSelectorInto = function($el) {
      var state = this.stateFor($el);
      var allRoutes = state.allRoutes;
      if (!allRoutes) {
        return;
      }
      var currentStop = state.stop;
      var routeData = [];
      allRoutes.routes.forEach(function(route) {
        routeData.push({
          id: route.route_id,
          text: route.route_name
        });
      });
      $el.find(this.attr.routeSelector).select2({
        data: routeData,
        width: '100%',
        placeholder: 'Other routes here...',
        multiple: true
      }).select2('val', currentStop.route_ids);
    };

    this.$closestStop = function(ev) {
      return $(ev.target).closest(this.attr.stopSelector);
    };

    this.onToggle = function(ev) {
      ev.preventDefault();
      var $stop = this.$closestStop(ev);
      var state = this.stateFor($stop);
      this.trigger($stop, 'uiToggleEditStop', {
        stop: state.stop
      });
    };

    this.onDelete = function(ev) {
      var $stop = this.$closestStop(ev);
      var state = this.stateFor($stop);
      this.trigger($stop, 'uiRemoveStop', {
        stop: state.stop
      });
    };

    this.onRoutesUpdated = function(ev) {
      var $stop = this.$closestStop(ev);
      var state = this.stateFor($stop);
      var currentStop = state.stop;
      currentStop.route_ids = ev.val;
      this.trigger($stop, 'uiUpdateStop', {
        stop: currentStop
      });
    };

    this.after('initialize', function() {
      this.on('click', {
        toggleSelector: this.onToggle,
        deleteSelector: this.onDelete
      });
      this.on('change', {
        routeSelector: this.onRoutesUpdated
      });
      this.on('uiToggleEditStop', 'uiRequestRoutesForStop');
    });
  }

});
