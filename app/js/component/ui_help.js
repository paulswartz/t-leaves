define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withCaching = require('component/with_caching');
  require('components-bootstrap/js/bootstrap');
  /**
   * Module exports
   */

  return defineComponent(uiHelp, withCaching);

  /**
   * Module function
   */

  function uiHelp() {
    this.attributes({
      jumbotronSelector: '.jumbotron',
      helpAlertSelector: '.help-alert',
      helpTextSelector: '.help-text',
      cachingPrefix: 'help-',
      helpHTML: {
        edit: '<strong>Great!</strong> If you want to track more than one route, or just remove the stop entirely, click the triangle to open the edit panel.',
        saving: 'Your changes are saved automatically; click the triangle again to close.',
        questions: 'If you have any questions or comments about T-Leaves, file an issue <a href="https://gitlab.com/paulswartz/t-leaves/issues" target="_blank">on GitLab</a> or send me an <a href="mailto:paulswartz@gmail.com" target="_blank">email</a>!'
      }
    });

    this.onAddedStop = function() {
      this.select('jumbotronSelector').remove();
      this.showHelp('edit');
    };

    this.onToggledEdit = function() {
      if (this.getCache('saving')) {
        this.showHelp('questions');
      } else {
        this.showHelp('saving');
      }
    };

    this.onRemoved = function() {
      this.showHelp('questions');
    };

    this.showHelp = function(helpType) {
      if (this.getCache(helpType)) {
        return;
      }
      this.setCache(helpType, true);
      this.select('helpTextSelector').html(this.attr.helpHTML[helpType]);
      this.select('helpAlertSelector').removeClass('hidden').show().alert();
    };

    this.after('initialize', function () {
      this.on(document, 'dataAddStop', this.onAddedStop);
      this.on(document, 'uiToggleEditStop', this.onToggledEdit);
      this.on(document, 'uiRemoveStop', this.onRemoved);
    });
  }

});
