define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');

  /**
   * Module exports
   */

  return defineComponent(uiSelect2Scroll);

  /**
   * Module function
   */

  function uiSelect2Scroll() {
    this.attributes({
      containerSelector: '.select2-container-active',
      dropdownSelector: 'body > .select2-drop-active'
    });

    this.scrollTop = function() {
      var width = $('body').width();

      if (width >= 768) {
        // probably don't need this workaround on larger screens
        return;
      }
      var $dropdown = $(this.attr.dropdownSelector);
      if (!$dropdown.length) {
        return;
      }

      var top = $dropdown.offset().top +
            $dropdown.outerHeight(false);

      var $container = $(this.attr.containerSelector);

      if ($container.length) {
        top = Math.min(top,
                       $container.offset().top +
                       $container.outerHeight(false));
      }

      var height = $('body').height();

      $('html, body').scrollTop(top - height + 40);
    };


    this.after('initialize', function () {
      this.on('select2-open', this.scrollTop);
      this.on('select2-focus', this.scrollTop);
      this.on('resize', this.scrollTop);
      this.on('orientationchange', this.scrollTop);
    });
  }

});
