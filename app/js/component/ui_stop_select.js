define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  require('select2/select2');

  /**
   * Module exports
   */

  return defineComponent(uiStopSelect);

  /**
   * Module function
   */

  function uiStopSelect() {
    this.attributes({
      loadingText: 'Loading...',
      placeholder: 'Search for a stop...',
      routePlaceholder: 'Search for a train, bus, or boat line...',
      searchInputPlaceholder: 'Type here to filter...',
      minimumInputLength: 0,
      width: '100%',
      routeSelector: '.route-control',
      stopSelector: '.stop-control'
    });

    this.onStopsForRoute = function(ev, data) {
      var directions = [];
      data.direction.forEach(function(direction) {
        var children = [];
        directions.push({
          text: direction.direction_name,
          children: children
        });
        direction.stop.forEach(function(stop) {
          children.push({
            id: stop.stop_id,
            text: stop.stop_name,
            stop_order: stop.stop_order
          });
        });

        children.sort(function(a, b) {
          a = a.stop_order;
          b = b.stop_order;
          if (a < b) {
            return -1;
          } else if (a > b) {
            return 1;
          }
          return 0;
        });
      });


      this.select('stopSelector').select2({
        data: directions,
        placeholder: this.attr.placeholder,
        searchInputPlaceholder: this.attr.searchInputPlaceholder,
        minimumInputLength: this.attr.minimumInputLength,
        width: this.attr.width
      });
      this.select('stopSelector').select2('open');
    };

    this.onRouteSelected = function(ev) {
      this.select('stopSelector').text(this.attr.loadingText);
      this.trigger('uiRequestStopsForRoute', {
        route: {
          route_id: ev.val
        }
      });
    };

    this.onStopSelected = function(ev) {
      this.select('stopSelector').select2('val', '');
      this.trigger('uiAddStop', {
        stop: {
          stop_id: ev.val,
          route_id: this.select('routeSelector').select2('val')
        }
      });
    };

    this.onRoutes = function(ev, data) {
      var modes = [];
      var modeHash = {};
      data.routes.forEach(function(route) {
        if (!modeHash[route.mode_name]) {
          modeHash[route.mode_name] = {
            text: route.mode_name,
            children: []
          };
          modes.push(modeHash[route.mode_name]);
        }
        modeHash[route.mode_name].children.push({
          id: route.route_id,
          text: route.route_name
        });
      });

      this.select('routeSelector').select2({
        data: modes,
        placeholder: this.attr.routePlaceholder,
        searchInputPlaceholder: this.attr.searchInputPlaceholder,
        minimumInputLength: this.attr.minimumInputLength,
        width: this.attr.width
      });
    };

    this.after('initialize', function () {
      this.on(document, 'dataStopsForRoute', this.onStopsForRoute);
      this.on(document, 'dataRoutes', this.onRoutes);
      this.on('change', {
        routeSelector: this.onRouteSelected,
        stopSelector: this.onStopSelected
      });

      this.select('routeSelector').text(this.attr.loadingText);

      window.setTimeout(function() {
        $(document).trigger('uiRequestStops');
        $(document).trigger('uiRequestRoutes');
      }, 0);
    });
  }

});
