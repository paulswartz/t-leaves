define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withMbtaTiming = require('component/with_mbta_timing');
  /**
   * Module exports
   */

  return defineComponent(withMbtaTiming, dataSchedule);

  /**
   * Module function
   */

  function dataSchedule() {
    this.attributes({
      listenEvent: 'uiRequestSchedule',
      triggerEvent: 'dataSchedule',
      apiEndpoint: 'schedulebystop',
      msBetweenRequests: 1000 * 60 * 10
    });
  }
});
