define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withMbtaApi = require('component/with_mbta_api');
  var withCaching = require('component/with_caching');

  /**
   * Module exports
   */

  return defineComponent(dataRoutes, withMbtaApi, withCaching);

  /**
   * Module function
   */

  function dataRoutes() {
    this.attributes({
      cacheKey: 'routes'
    });

    // Subway routes don't have the best names, so we use these as a mapping
    // to a better name
    var routeName = {
      '810_': 'Green Line (B: Boston College - Lechmere)',
      '813_': 'Green Line (B: Boston College - Park St)',
      '823_': 'Green Line (C: Chestnut Hill - Park Street)',
      '830_': 'Green Line (C: Cleveland Circle - Lechmere)',
      '831_': 'Green Line (C: Cleveland Circle - North Station)',
      '840_': 'Green Line (D: Reservoir - Lechmere)',
      '842_': 'Green Line (D: Reservoir - Park Street)',
      '851_': 'Green Line (D: Riverside - North Station)',
      '852_': 'Green Line (D: Riverside - Park Street)',
      '880_': 'Green Line (E: Heath - Lechmere)',
      '882_': 'Green Line (E: Heath - Park St)',
      '946_': 'Blue Line (Orient Heights)',
      '948_': 'Blue Line (Wonderland)',
      '903_': 'Orange Line (Oak Grove)',
      '913_': 'Orange Line (Wellington)',
      '931_': 'Red Line (Ashmont)',
      '933_': 'Red Line (Braintree)'
    };

    this.onRequestRoutes = function() {
      var cached = this.getCache(this.attr.cacheKey);
      if (cached) {
        this.onRoutes(cached);
      } else {
        this.getMbtaApi('routes', {
          success: this.onRoutes
        });
      }
    };

    this.onRequestRoutesForStop = function(ev, data) {
      var cached = this.getCache(this.attr.cacheKey + '-' + data.stop.stop_id);
      if (cached) {
        this.onRoutesForStop(cached);
      } else {
        this.getMbtaApi('routesbystop', {
          stop: data.stop.stop_id,
          success: this.onRoutesForStop
        });
      }
    };

    this.onRoutes = function(data) {
      this.setCache(this.attr.cacheKey, data, 60 * 60 * 24);
      data = $.extend({}, data);
      var mode = data.mode;
      delete data.mode;
      data.routes = this.serializeRoutes({
        mode: mode
      });
      this.trigger('dataRoutes', data);
    };

    this.onRoutesForStop = function(data) {
      this.setCache(this.attr.cacheKey + '-' + data.stop_id,
                    data,
                    60 * 60 * 24);
      data = $.extend({}, data);
      var mode = data.mode;
      delete data.mode;
      data.routes = this.serializeRoutes({
        mode: mode
      });
      data.stop = {
        stop_id: data.stop_id,
        stop_name: data.stop_name
      };
      delete data.stop_id;
      delete data.stop_name;
      this.trigger('dataRoutesForStop', data);
    };

    this.serializeRoutes = function(data) {
      var routes = [];
      data.mode.forEach(function(mode) {
        mode.route.forEach(function(route) {
          if (route.route_hide === 'true') {
            return;
          }
          routes.push({
            route_id: route.route_id,
            route_name: routeName[route.route_id] || route.route_name,
            route_type: mode.route_type,
            mode_name: mode.mode_name
          });
        });
      });
      return routes;
    };



    this.after('initialize', function () {
      this.on(document, 'uiRequestRoutes', this.onRequestRoutes);
      this.on(document, 'uiRequestRoutesForStop', this.onRequestRoutesForStop);
    });
  }

});
