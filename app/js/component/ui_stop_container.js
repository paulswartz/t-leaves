define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withChildComponents = require(
    'flight-with-child-components/lib/flight-with-child-components');
  var uiStop = require('component/ui_stop');

  /**
   * Module exports
   */

  return defineComponent(uiStopContainer, withChildComponents);

  /**
   * Module function
   */

  function uiStopContainer() {
    this.attributes({
      stopClasses: 'col-xs-12 col-sm-6 col-md-4 stop'
    });

    this.onAddStop = function(ev, data) {
      var newDiv = $('<div></div>')
            .addClass(this.attr.stopClasses);
      this.$node.append(newDiv);
      this.attachChild(uiStop, newDiv, {
        stop: data.stop
      });
    };

    this.after('initialize', function () {
      this.on(document, 'dataAddStop', this.onAddStop);
    });
  }

});
