define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withMbtaApi = require('component/with_mbta_api');
  var withCaching = require('component/with_caching');

  /**
   * Module exports
   */

  return defineComponent(dataStops, withMbtaApi, withCaching);

  /**
   * Module function
   */

  function dataStops() {
    this.attributes({
      cacheKey: 'stop-'
    });

    this.onRequestStopsForRoute = function(ev, data) {
      var route = data.route;
      var cached = this.getCache(this.attr.cacheKey + route.route_id);
      if (cached) {
        this.onStopsForRoute(cached, route);
      } else {
        this.getMbtaApi('stopsbyroute', {
          route: route.route_id,
          success: function(data) {
            this.onStopsForRoute(data, route);
          }
        });
      }
    };

    this.onStopsForRoute = function(data, route) {
      this.setCache(this.attr.cacheKey + route.route_id, data,
                    60 * 60 * 24);
      data.direction.forEach(function(direction) {
        direction.stop.forEach(function(stop) {
          stop.stop_order = parseInt(stop.stop_order, 10);
        });
      });
      this.trigger('dataStopsForRoute', data);
    };

    this.after('initialize', function() {
      this.on(document, 'uiRequestStopsForRoute', this.onRequestStopsForRoute);
    });
  }

});
