define(function (require) {

  'use strict';

  var compose = require('flight/lib/compose');
  var withMbtaApi = require('component/with_mbta_api');
  /**
   * Module exports
   */

  return withMbtaTiming;

  /**
   * Module function
   */

  // simple function to prevent us from calling the API more than once every
  // limit milliseconds
  function debounce(f, limit, t) {
    t = t || this;
    return (function() {
      var ignoring = {};
      return function() {
        var key = JSON.stringify(Array.prototype.slice.call(arguments));
        if (!ignoring[key]) {
          ignoring[key] = true;
          setTimeout(function() {
            ignoring[key] = false;
          }, limit);
          return f.apply(this, arguments);
        }
        return undefined;
      }.bind(this);
    }.call(t));
  }


  function withMbtaTiming() {
    compose.mixin(this, [withMbtaApi]);

    this.attributes({
      listenEvent: null,
      triggerEvent: null,
      apiEndpoint: null,
      apiData: {},
      msBetweenRequests: null,
      dataKey: 'timing'
    });

    this.onTiming = function(data, stop) {
      this.cachedTimings[data.stop_id] = data;
      var response = {};
      response[this.attr.dataKey] = this.serializeTiming(data, stop);
      this.trigger(document, this.attr.triggerEvent, response);
    };

    this.onRequestTiming = function(ev, data) {
      var stop = data.stop;
      var stopId = stop.stop_id;
      this.debouncedMbtaApi(this.attr.apiEndpoint, $.extend({
        stop: stopId,
        success: function(data) {
          this.onTiming(data, stop);
        }.bind(this)
      }, this.attr.apiData));
      if (this.cachedTimings[stopId]) {
        this.onTiming(this.cachedTimings[stopId], stop);
      }
    };

    this.serializeTiming = function(timing, stop) {
      // make a copy of the timing data so we can modify it
      timing = $.extend(true, {}, timing);
      timing.mode.forEach(function(mode) {
        var routes = [];
        mode.route.forEach(function(route) {
          // remove routes which the user doesn't care about
          if (stop.route_ids.indexOf(route.route_id) > -1) {
            routes.push(route);
          }
        });
        mode.route = routes;
      });
      return timing;
    };

    this.after('initialize', function () {
      this.cachedTimings = {};
      this.debouncedMbtaApi = debounce(
        this.getMbtaApi,
        this.attr.msBetweenRequests,
        this);
      this.on(document, this.attr.listenEvent,
              this.onRequestTiming);

    });
  }

});
