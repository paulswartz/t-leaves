define(function (require) {

  'use strict';

  /**
   * Module dependencies
   */

  var defineComponent = require('flight/lib/component');
  var withLocalStorage = require('flight-storage/lib/adapters/local-storage');

  /**
   * Module exports
   */

  return defineComponent(dataSavedStops, withLocalStorage);

  /**
   * Module function
   */

  function dataSavedStops() {
    this.attributes({
      storageTag: 'user-stops'
    });

    this.onAddStop = function(ev, data) {
      var stopId = data.stop.stop_id;
      if (this.stops[stopId]) {
        this.onUpdateStop(ev, data);
        return;
      }
      this.stops[stopId] = {
        stop_id: stopId,
        route_ids: [data.stop.route_id]
      };
      this.save();
      this.triggerAddStop(stopId);
    };

    this.onUpdateStop = function(ev, data) {
      var stopId = data.stop.stop_id;
      if (data.stop.route_ids) {
        // full update
        this.stops[stopId] = data.stop;
        this.save();
        this.triggerUpdateStop(stopId);
      } else {
        var routeId = data.stop.route_id;
        var stop = this.stops[stopId];
        if (stop.route_ids.indexOf(routeId) === -1) {
          // not found
          stop.route_ids.push(routeId);
          this.triggerUpdateStop(stopId);
          this.save();
        }
      }
    };

    this.onRemoveStop = function(ev, data) {
      var stopId = data.stop.stop_id;
      if (!this.stops[stopId]) {
        // gone already; ignore
        return;
      }
      delete this.stops[stopId];
      this.trigger(document, 'dataRemoveStop', data);
      this.save();
    };

    this.load = function() {
      this.stops = this.get(this.attr.storageTag) || {};

      var stopId;
      for (stopId in this.stops) {
        if (this.stops.hasOwnProperty(stopId)) {
          this.triggerAddStop(stopId);
        }
      }
    };

    this.save = function() {
      this.set(this.attr.storageTag, this.stops);
    };

    this.triggerAddStop = function(stopId) {
      this.trigger(document, 'dataAddStop', {
        stop: this.stops[stopId]
      });
    };

    this.triggerUpdateStop = function(stopId) {
      this.trigger(document, 'dataUpdateStop', {
        stop: this.stops[stopId]
      });
    };

    this.after('initialize', function () {
      this.load();
      this.on('uiAddStop', this.onAddStop);
      this.on('uiUpdateStop', this.onUpdateStop);
      this.on('uiRemoveStop', this.onRemoveStop);
    });
  }

});
