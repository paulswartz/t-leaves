'use strict';

var gulp = require('gulp');
var refresh = require('gulp-livereload');
var livereload = require('tiny-lr');
var server = livereload();
var compiler = require('gulp-hogan-compile');
var rjs = require('gulp-requirejs');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var processhtml = require('gulp-processhtml');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer-core');
var cssmin = require('gulp-minify-css');
var htmlmin = require('gulp-minify-html');
var concat = require('gulp-concat');
var revAll = require('gulp-rev-all');

gulp.task('livereload-server', function () {
  server.listen(35729, function (err) {
    if (err) { return console.log(err); }
  });
});

gulp.task('css', function () {
  gulp.src('app/less/**/*.less')
    .pipe(less())
    .pipe(postcss([autoprefixer({
      from: 'foo',
      to: 'bar'
    }).postcss]))
    .pipe(gulp.dest('app/css'))
    .pipe(refresh(server));
});

gulp.task('js', function () {
  gulp.src('app/**/*.js').pipe(refresh(server));
});

gulp.task('html', function () {
  gulp.src('app/templates/*.html')
    .pipe(compiler('compiled.js'))
    .pipe(gulp.dest('app/templates/'))
    .pipe(refresh(server));
});

gulp.task('default', ['css', 'js', 'html'], function () {
  gulp.run('livereload-server');

  gulp.watch('app/**/*.less', function () {
    gulp.run('css');
  });

  gulp.watch('app/**/*.js', function () {
    gulp.run('js');
  });

  gulp.watch('app/**/*.html', function () {
    gulp.run('html');
  });
});

gulp.task('build', ['css', 'js', 'html'], function() {
  rjs({
    name: '../js/main',
    mainConfigFile: './app/js/main.js',
    baseUrl: './app/bower_components/',
    findNestedDependencies: true,
    include: 'requirejs/require',
    out: 'main.js'
  })
    .pipe(uglify())
    .pipe(gulp.dest('./build/js'));

  gulp.src('app/bower_components/es5-shim/es5-sh?m.js')
    .pipe(concat('es5-shim-sham.js', {newLine: ';'}))
    .pipe(uglify())
    .pipe(gulp.dest('build/js'));

  gulp.src('app/css/all.css')
    .pipe(cssmin({
      cache: false,
      compatibility: 'ie7'
    }))
    .pipe(gulp.dest('./build/css'));

  gulp.src('app/bower_components/select2/select*.png')
    .pipe(gulp.dest('./build/css'));
  gulp.src('app/bower_components/select2/select2-spinner.gif')
    .pipe(gulp.dest('./build/css'));

  gulp.src('app/img/*')
    .pipe(gulp.dest('./build/img'));

  gulp.src('app/index.html')
    .pipe(processhtml('index.html'))
    .pipe(htmlmin({
      quotes: true,
      conditionals: true
    }))
    .pipe(gulp.dest('./build'));
});

gulp.task('rev', function() {
  gulp.src(['build/index.html', 'build/js/*', 'build/css/*', 'build/img/*'],
           {base: 'build'})
    .pipe(revAll({
      ignore: [/index.html/]
    }))
    .pipe(gulp.dest('dist'));

  gulp.src('.htaccess')
    .pipe(gulp.dest('dist'));
});
